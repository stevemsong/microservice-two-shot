from django.db import models
from django.urls import reverse

# Create your models here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_URL = models.URLField(max_length=200)

    location = models.ForeignKey(
        LocationVO,
        related_name='hats',
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("show_hat", kwargs={"id": self.id})
