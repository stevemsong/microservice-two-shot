from common.json import ModelEncoder
from django.http import JsonResponse
from hats_rest.models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
import json

# encoders


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "import_href",
    ]


class HatListEncoder(ModelEncoder):
    model = (Hat)
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_URL",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = (Hat)
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_URL",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
        }


# views


@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )

    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location ID"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})
    else:
        content = json.loads(request.body)
        Hat.objects.filter(id=id).update(**content)
        hat - Hat.objects.filter(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
