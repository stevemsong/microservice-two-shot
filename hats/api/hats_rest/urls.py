from django.urls import path
from hats_rest.views import list_hats, show_hat


urlpatterns = [
    path("", list_hats, name="list_hats"),
    path("<int:id>/", show_hat, name="show_hat"),
]
