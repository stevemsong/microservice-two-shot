# Wardrobify

Team:

* Steve Song - Shoes
* Nick Willcox - Hats

## Design

## Shoes microservice

A Shoe model and BinVO model were made in order to implement CRUD functionality for shoe creation. The BinVO model was needed so that the wardobe microservice data coming from the poller could be used.

## Hats microservice

I created a Hat model and linked it to the wardrobe via a Location Value Object. Hats have create and delete functionality. The front end displays all of the hat properties and allows a delete from the list view. The poller communicates with the wardrobe API to connect location information to each instance of "hat".
