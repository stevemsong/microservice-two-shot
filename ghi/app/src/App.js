import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import CreateShoeForm from './CreateShoeForm';
import HatForm from './HatForm';
import HatList from './HatList';

function App(props) {
  // if (props.hats === undefined) {
  //   return null;
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoeList shoes={props.shoes} />} />
          <Route path="shoes">
            <Route path="new" element={<CreateShoeForm />} />
          </Route>
          <Route path="hats">
            <Route index element={<HatList hats={props.hats}/>}/>
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
