import React, {useEffect, useState} from 'react'


function CreateShoeForm () {
    const [bins, setBins] = useState([])
    const [formData, setFormData] = useState({
        manufacturer: '',
        model: '',
        color: '',
        url: '',
        bin: '',
    })
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        let binhref=formData.bin
        const url = `http://localhost:8080${binhref}shoes/`;

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
          setFormData({
            manufacturer: '',
            model: '',
            color: '',
            url: '',
            bin: '',
          });
        }
      }
      const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input value={formData.model} onChange={handleFormChange} placeholder="Model" required type="text" name="model" id="model" className="form-control"/>
                <label htmlFor="model">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.manufacturer} onChange={handleFormChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.color} onChange={handleFormChange} placeholder="Color" type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.url} onChange={handleFormChange} placeholder="Url" required type="text" name="url" id="url" className="form-control"/>
                <label htmlFor="url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select value={formData.bin} onChange={handleFormChange} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {bins.map(bin => {
                    return(
                        <option key={bin.href} value={bin.href}>{bin.bin_number}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>

    )

}
export default CreateShoeForm
