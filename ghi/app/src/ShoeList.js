function ShoeList(props) {
    async function DeleteShoe(id) {
        const deleteURL = `http://localhost:8080/api/shoes/${id}/`;
        const fetchConfig = {
            method: "delete",
        };
        const shoeDelete = await fetch(deleteURL, fetchConfig);
        if (shoeDelete.ok) {
            window.location.reload();
        }
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>Bin</th>
                    <th>Image</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
            {props.shoes.map(shoe => {
                return (
                <tr key={shoe.href}>
                    <td className="col-3">{shoe.model}</td>
                    <td className="col-3">{shoe.manufacturer}</td>
                    <td className="col-3">{shoe.color}</td>
                    <td>{shoe.bin.bin_number}</td>
                    <td><img src={shoe.url} className="img-fluid"></img></td>
                    <td>
                        <button onClick={() => DeleteShoe(shoe.id)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
    )
}


export default ShoeList
