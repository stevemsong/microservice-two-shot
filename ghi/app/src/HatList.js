import React, { useEffect, useState } from 'react';


function HatList() {
    const [list, setList] = useState([]);

    async function loadHats() {
        const response = await fetch('http://localhost:8090/hats/');
        if (response.ok) {
          const data = await response.json();
          console.log(data)
          setList(data.hats)
        }
    }

    async function deleteHat(id) {
        const deleteURL = `http://localhost:8090/hats/${id}/`;
        const fetchConfig = {
            method: "DELETE",
        };
        const hatDelete = await fetch(deleteURL, fetchConfig);
        if (hatDelete.ok) {
            window.location.reload();
        }
    }

    useEffect(() => {
        loadHats();
      }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Location</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {list.map(hat => {
                    return (
                        <tr key={ hat.id }>
                            <td><img src={ hat.picture_URL } alt="hat" width="300" height="300"/></td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.location.name }</td>
                            <td>
                                <button onClick={() => deleteHat(hat.id)}>Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatList;